package net.bergice.swiftbooks.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import butterknife.InjectView;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.base.torrent.TorrentFetcherKickassSo;
import net.bergice.swiftbooks.base.torrent.TorrentHeader;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.imageloader.ImageLoader;
import net.bergice.swiftbooks.utils.Utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/** Adapter for filling the book finder grid view */
public class BookBrowserAdapter extends ObservableAdapter
{
    private LayoutInflater inflater;
	private Context context;

    private List<BookPreviewItem> items = new ArrayList<BookPreviewItem>();
	
	private static final int	BOOK_REQUEST_AMOUNT		=	10;	// How many books to ask for per request.
	private TorrentFetcherKickassSo torrentFetcher;

    public BookBrowserAdapter(Context context)
    {
    	this.context = context;
        inflater = LayoutInflater.from(context);

        OnCreate ();

    }

    public void requestMoreData () {
    	if (Utils.isNetworkAvailable(context)) {
        	torrentFetcher.getTopASync(BOOK_REQUEST_AMOUNT);
    	} else {
    		Toast toast = Toast.makeText(context, "No internet found :(\nCheck your connection please.", Toast.LENGTH_LONG);
    		toast.show();
    	}
    }

	public void OnCreate () {
    	torrentFetcher = new TorrentFetcherKickassSo(this);
    	requestMoreData();
    }
    
    // Received new books, add to list.
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1.getClass() == TorrentHeader.class) {
			TorrentHeader header = (TorrentHeader)arg1;
	    	items.add(new BookPreviewItem(header));
	    	System.out.println("Added item: " + header.title);
	    	changed();
		} else {
			System.out.println("Problem updating book, unknown class type " + arg1.getClass().toString());
		}
	}
	
    private void changed() {
    	Message msg = new Message();
    	msg.setTarget(handler);
    	msg.sendToTarget();
	}

	Handler handler = new Handler(Looper.getMainLooper()) {
		@Override
		public void handleMessage(Message msg) {
	    	try{
				 notifyDataSetChanged();
	        }
	        catch(NullPointerException e)
	        {
				e.printStackTrace();
	        }
		}
	};

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i)
    {
        return items.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return items.get(i).drawableId;
    }

    static class ViewHolder {
//      @InjectView(R.id.picture) 
      ImageView picture;
//      @InjectView(R.id.text) 
      TextView text;
//      @InjectView(R.id.text_author) 
      TextView text_author;
//      @InjectView(R.id.pages) 
      ImageView pages;
//      @InjectView(R.id.loading) 
      ProgressBar loading;

      public ViewHolder(View view) {
    	  picture = (ImageView) view.findViewById(R.id.picture);
    	  text = (TextView) view.findViewById(R.id.text);
    	  text_author = (TextView) view.findViewById(R.id.text_author);
    	  pages = (ImageView) view.findViewById(R.id.pages);
    	  loading = (ProgressBar) view.findViewById(R.id.loading);
//        ButterKnife.inject(this, view);
      }
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
    	ViewHolder holder;
        BookPreviewItem item = (BookPreviewItem)getItem(position);
        
        // If view has not been already initialized.
        if(view == null)
        {
        	// Inflate view
           view = inflater.inflate(R.layout.single_book_item, parent, false);
           holder = new ViewHolder(view);
           view.setTag(holder);

           // Fade in view
           final Animation animationFadeIn = AnimationUtils.loadAnimation(context, R.anim.fadein);
           view.startAnimation(animationFadeIn);
        } else {
        	// Fetch view
            holder = (ViewHolder) view.getTag();
        }
   		holder.loading.setVisibility(View.GONE);
//   		holder.picture.setVisibility(View.GONE);

//   		holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        holder.pages.getLayoutParams().width = holder.picture.getWidth();

        holder.text.setText(item.name);
        holder.text_author.setText(item.author);

        // Load cover picture from URL
        if (item.cover!="") {
          ImageLoader imgLoader = new ImageLoader(context);
//          imgLoader.DisplayImage(item.cover, holder.loading, holder.picture);
          imgLoader.DisplayImage(item.cover, -1, holder.picture);
        } else {
        	holder.picture.setImageResource(item.drawableId);
        }

        return view;
    }
}