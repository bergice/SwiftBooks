package net.bergice.swiftbooks.dialogs;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.DownloadTask;
import net.bergice.swiftbooks.utils.imageloader.ImageLoader;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BookPreviewDialog extends Dialog
//implements android.view.View.OnClickListener 
{
	
	private BookPreviewItem book;
	private Context context;

	public BookPreviewDialog(Context context, BookPreviewItem book) {
		super(context);
		this.context = context;
		this.book = book;
        setContentView(R.layout.book_preview);
        setTitle(book.name);
        fillInfo();
	}

	private void fillInfo() {
//		View layout = (View) findViewById(R.layout.book_preview);
		ViewGroup layout = (ViewGroup)getWindow().getDecorView();
		
		TextView book_title = (TextView) layout.findViewById(R.id.book_title);
		book_title.setText(book.name);

		TextView book_author = (TextView) layout.findViewById(R.id.book_author);
		book_author.setText(book.author);

		ImageView book_cover = (ImageView) layout.findViewById(R.id.book_cover);
		book_cover.setImageResource(book.drawableId);

        ImageLoader imgLoader = new ImageLoader(context);
        imgLoader.DisplayImage(book.cover, -1, book_cover);

		TextView torrent_url = (TextView) layout.findViewById(R.id.torrent_url);
		torrent_url.setText(book.torrentFileURL);

		ImageButton torrent_download = (ImageButton) findViewById(R.id.torrent_download);
		torrent_download.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onTorrentDownloadButtonClicked ();
			}
		});
	}

	protected void onTorrentDownloadButtonClicked() {
		// Loading Dialog. This should be put as an item into the SideBar under Downloads Section as downloads are done in the background.
		ProgressDialog mProgressDialog;
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setMessage("Downloading torrent...");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setCancelable(true);
		mProgressDialog.show();

//		// execute this when the downloader must be fired
//		final DownloadTask downloadTask = new DownloadTask(context);
//		downloadTask.execute(book.torrentFileURL);
//
//		mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//		    @Override
//		    public void onCancel(DialogInterface dialog) {
//		        downloadTask.cancel(true);
//		    }
//		});
//
//	    onBackPressed();
	}

//	@Override
//	public void onClick(View v) {
//		
//		switch (v.getId()) {
//		
////			case R.id.btn_yes:
////				c.finish();
////			break;
////			case R.id.btn_no:
////				dismiss();
////				break;
//			default:
//			break;
//			
//		}
//		
//		dismiss();
//	}
}
