package net.bergice.swiftbooks.services.bookservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import net.bergice.swiftbooks.base.torrent.TorrentFetcherKickassSo;
import net.bergice.swiftbooks.base.torrent.TorrentHeader;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.Utils;

public class BookService extends Service implements Observer {

	private Context context;
	private static final int	BOOK_REQUEST_AMOUNT		=	10;	// How many books to ask for per request.
	private TorrentFetcherKickassSo torrentFetcher;

    private List<BookPreviewItem> items = new ArrayList<BookPreviewItem>();
    DatabaseHandler database;

	public BookService (Context context) {
		this.context = context;
		database = new DatabaseHandler(context);
    	torrentFetcher = new TorrentFetcherKickassSo(this);
    	requestMoreData();
	}

    public void requestMoreData () {
    	if (Utils.isNetworkAvailable(context)) {
        	torrentFetcher.getTopASync(BOOK_REQUEST_AMOUNT);
    	} else {
    		Toast toast = Toast.makeText(context, "No internet found :(\nCheck your connection please.", Toast.LENGTH_LONG);
    		toast.show();
    	}
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1.getClass() == TorrentHeader.class) {
			TorrentHeader header = (TorrentHeader)arg1;
	    	items.add(new BookPreviewItem(header));
	    	System.out.println("Added item: " + header.title);
		} else {
			System.out.println("Problem updating book, unknown class type " + arg1.getClass().toString());
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
