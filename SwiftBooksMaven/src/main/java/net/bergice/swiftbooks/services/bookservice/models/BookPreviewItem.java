package net.bergice.swiftbooks.services.bookservice.models;

import java.io.Serializable;
import java.util.List;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.base.torrent.TorrentHeader;
import net.bergice.swiftbooks.services.bookservice.SQLTable;

/** Contains minimum information for a book preview to showcase it and to download the torrent as well as fetch more information. */
public class BookPreviewItem extends SQLTable implements Serializable {
	private static final long serialVersionUID = 1L;

	public final String name;
	public final String author;
	public final int drawableId;
	public final String cover;
	public final String torrentFileURL;
//	public List<TorrentHeader> torrentHeaders;
	
	public final String amazonURL = "http://www.amazon.com/Feast-Crows-Song-Fire-Thrones/dp/055358202X/ref=sr_1_5?s=books&ie=UTF8&qid=1422992042&sr=1-5&keywords=game+of+thrones";
	public final String torrentURL = "https://kickass.so/a-feast-for-crows-a-song-of-ice-and-fire-t7043623.html";
	public final String description = "Empty";

    public BookPreviewItem (TorrentHeader header) {
        this.author = header.author;
    	this.cover = header.cover;
        this.name = header.title;
        this.drawableId = R.drawable.cover;
    	this.torrentFileURL = header.download;
//    	this.torrentHeaders = header.torrentHeaders;
    }

    private static final String TABLE_NAME = "book_headers";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_COVER = "cover";
    private static final String KEY_TORRENT_FILE_URL = "torrentFileURL";
    private static final String KEY_DESCRIPTION = "desc";

	public static String getTable() {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_AUTHOR + " TEXT," 
                + KEY_COVER + " TEXT,"
                + KEY_TORRENT_FILE_URL + " TEXT,"
                + KEY_DESCRIPTION + " TEXT" + ")";
        return CREATE_TABLE;
	}

	public static String getTableName() {
		return TABLE_NAME;
	}

}
