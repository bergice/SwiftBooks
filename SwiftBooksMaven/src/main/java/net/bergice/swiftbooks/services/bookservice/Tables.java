package net.bergice.swiftbooks.services.bookservice;

import java.util.ArrayList;
import java.util.List;

public class Tables {

	public static abstract class Table {
		public String name;
		public String url;
		public static final String URLPageMarker = "\\p";
		public int minElements = 0;
		public int maxElements = 100;
		public abstract void getRoutine();
	}

	public static class AmazonTable extends Table {
		public int maxBookCovers = 50;

		public AmazonTable () {
			super();
		}
		
		@Override
		public void getRoutine() {
			
		}
		
	}

	private static List<AmazonTable> tables = new ArrayList<AmazonTable>();
	private static AmazonTable all_top;
	
	static {
		all_top = new AmazonTable();
		
		tables.add(all_top);
	}
	
	public static List<AmazonTable> getTables () {
		return tables;
	}
}
