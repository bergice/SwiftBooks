package net.bergice.swiftbooks.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class FixedAspectRatioImageView extends ImageView
{
    public FixedAspectRatioImageView(Context context)
    {
        super(context);
    }

    public FixedAspectRatioImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FixedAspectRatioImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Scale to new dimensions but maintain aspect ratio.
        Bitmap bitmap = ((BitmapDrawable)getDrawable()).getBitmap();
        if (bitmap!=null) {
	        int bitmapWidth = bitmap.getWidth();
	        int bitmapHeight = bitmap.getHeight();
	        float aspectRatio = (float)bitmapHeight/(float)bitmapWidth;
	        int measuredWidth = getMeasuredWidth();
//	        int measuredHeight = getMeasuredHeight();
	        int finalHeight = (int)(measuredWidth*aspectRatio);
	        setMeasuredDimension(measuredWidth, finalHeight);
//	        getParent().
        }
        else {
        	setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
        }
	}
}