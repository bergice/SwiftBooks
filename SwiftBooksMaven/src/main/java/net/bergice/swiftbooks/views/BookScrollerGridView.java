package net.bergice.swiftbooks.views;

import net.bergice.swiftbooks.adapters.ObservableAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;

/** GridView based class that takes an ObservableAdapter and notifies the observer when the user scrolls to the bottom of the GridView.
 * @author bergice */
public class BookScrollerGridView extends GridView {
	ObservableAdapter adapter;

	public BookScrollerGridView(Context context) {
		super(context);
	}

	public BookScrollerGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BookScrollerGridView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setImageTextAdapter(ObservableAdapter adapter) {
		setAdapter(adapter);
		this.adapter = adapter;
	}
	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		View view = (View) getChildAt(getChildCount()-1);
		if (view!=null) {
			int diff = (view.getBottom()-getHeight()+getScrollY());
			if (diff == 0) {
				if (adapter!=null) {
					adapter.requestMoreData(); 
				}
			}
		}
		super.onScrollChanged(l, t, oldl, oldt);
	}

}
