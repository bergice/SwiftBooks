package net.bergice.swiftbooks.activities;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.caverock.androidsvg.SVGImageView;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.utils.Utils;
import net.bergice.swiftbooks.services.bookservice.DatabaseContactHandler;
import net.bergice.swiftbooks.services.bookservice.models.Contact;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class SplashActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_view);
		ViewGroup layout = (ViewGroup)getWindow().getDecorView();

		// Load background SVG overlay
		SVGImageView svgImageView = new SVGImageView(this);
		svgImageView.setImageResource(R.raw.overlay);
        svgImageView.setAlpha(0.5f);
        svgImageView.setScaleType(ScaleType.CENTER_CROP);
        layout.addView(svgImageView,
                       new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        Utils.sendViewToBack(svgImageView);

		ImageView logo = (ImageView) findViewById(R.id.logo);
//		fadeIn(layout);
		fadeIn(logo);

		
		
		boolean isFirstRun = Utils.isFirstTimeRun(this);
		DatabaseContactHandler db = new DatabaseContactHandler(this);

        db.addContact(new Contact("Ravi", "9100000000"));        
        db.addContact(new Contact("Srinivas", "9199999999"));
        db.addContact(new Contact("Tommy", "9522222222"));
        db.addContact(new Contact("Karthik", "9533333333"));
        db.emptyTable();
        
        List<Contact> contacts = db.getAllContacts();
        int tableSize = contacts.size();
        
        for (Contact cn : contacts) {
            System.out.println("Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber());
        }
            
//		int tableSize = db.getContactsCount();
		System.out.println("DATAINFO tableSize: " + tableSize);
		if (!isFirstRun || tableSize==0) {
			
		}
	}
	
	/** Called when the screen has faded in completely. */
	private void onAnimationComplete() {
		final long POST_DELAY = 2000;
		
		new Timer().schedule(new TimerTask() {          
		    @Override
		    public void run() {
		        Intent i = new Intent(SplashActivity.this, PageViewActivity.class);
		        startActivity(i);
		        finish();
			}
		}, POST_DELAY);
	}

	/** Fades in the screen using animations. */
	private void fadeIn(final View view) {
		final long PRE_DELAY = 1000;
		final long ANIMATION_DURATION = 600;
		
		view.setVisibility(View.INVISIBLE);
		new Timer().schedule(new TimerTask() {          
		    @Override
		    public void run() {
				runOnUiThread(new Runnable() {
				    @Override
				    public void run() {
				        // run AsyncTask here.  
//						Animation scale = new ScaleAnimation(1, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//						scale.setDuration(ANIMATION_DURATION);				    	

				    	AlphaAnimation alpha = new AlphaAnimation(0, 1);
				    	alpha.setDuration(ANIMATION_DURATION);

				    	alpha.setAnimationListener(new AnimationListener() {
							@Override
							public void onAnimationEnd(android.view.animation.Animation arg0) {
						        onAnimationComplete();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
							}
							
							@Override
							public void onAnimationStart(Animation animation) {
							}
						});

				    	AnimationSet animSet = new AnimationSet(true);
				    	animSet.setFillEnabled(true);
//				    	animSet.addAnimation(scale);
				    	animSet.addAnimation(alpha);
				    	
				    	// Launching animation set
				    	view.startAnimation(animSet);
				    	view.setVisibility(View.VISIBLE);
			    	}
				});
			}
		}, PRE_DELAY);
	}
}
