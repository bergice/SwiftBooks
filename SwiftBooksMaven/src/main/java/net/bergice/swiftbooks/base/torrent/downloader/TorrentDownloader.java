package net.bergice.swiftbooks.base.torrent.downloader;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

import com.turn.ttorrent.client.Client.ClientState;
import com.turn.ttorrent.client.SharedTorrent;

public class TorrentDownloader {
	private void onComplete () {
		
	}

	private void download(String file_url, String output_directory, final TorrentDownloadCompleteCallback callback) throws UnknownHostException, IOException {
		com.turn.ttorrent.client.Client client = new com.turn.ttorrent.client.Client(
				InetAddress.getLocalHost(),
				SharedTorrent.fromFile(
						new File(file_url),
						new File(output_directory)));

		client.setMaxDownloadRate(0.0);
		client.setMaxUploadRate(0.0);

		client.download();

		client.waitForCompletion();

		client.addObserver(new Observer(){

		@Override
		public void update(Observable o, Object arg) {
			ClientState state = (ClientState) arg;
			callback.onStateChanged(state);
		}});
	}
}
