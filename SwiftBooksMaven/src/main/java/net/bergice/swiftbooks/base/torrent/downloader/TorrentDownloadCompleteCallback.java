package net.bergice.swiftbooks.base.torrent.downloader;

import com.turn.ttorrent.client.Client.ClientState;

public interface TorrentDownloadCompleteCallback {
	public void onStateChanged (ClientState state);
}
