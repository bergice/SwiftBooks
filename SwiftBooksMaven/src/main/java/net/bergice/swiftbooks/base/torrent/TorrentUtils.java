package net.bergice.swiftbooks.base.torrent;

public class TorrentUtils {
    public enum TorrentProvider {
    	KickassSo,
    	AmazonTop
    }

    public static String getTorrentListURL (TorrentProvider provider, int page, boolean filterSeeders) {
    	String url = "";
    	switch (provider) {
		case KickassSo:
	    	url = "https://kickass.to/usearch/category%3Abooks/"+page+"/";
	    	if (filterSeeders) {
	    		url+="?field=seeders&sorder=desc";
	    	}
			break;
		case AmazonTop:
	    	url = "http://www.amazon.com/gp/bestsellers/books/ref=sv_b_2#"+page;
			break;
		default:
			break;
    	}
    	return url;
    }
}
