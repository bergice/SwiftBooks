package net.bergice.swiftbooks.base.torrent;

import java.util.List;

public class TorrentHeader {
	public String title = "";
	public String url = "";
	public String download = "";
	public String cover = "";
	public String author = "";
	public List<TorrentHeader> torrentHeaders;
}
