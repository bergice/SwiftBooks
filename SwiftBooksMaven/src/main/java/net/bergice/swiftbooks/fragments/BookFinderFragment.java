package net.bergice.swiftbooks.fragments;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.adapters.BookBrowserAdapter;
import net.bergice.swiftbooks.dialogs.BookPreviewDialog;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.Utils;
import net.bergice.swiftbooks.views.BookScrollerGridView;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;
import butterknife.ButterKnife;

/** Fragment for finding books online. Layout based on GridView.
 * TODO: Make BaseBookViewerFragment and extend BookFinderFragment and BookLibraryFragment from the base class.
 * @author bergice */
public class BookFinderFragment  extends Fragment {
//	@InjectView(R.id.gridview) 
	BookScrollerGridView gridview;

	Activity activity;
	Context context;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		FragmentActivity    faActivity  = (FragmentActivity)    super.getActivity();
	    LinearLayout        llLayout    = (LinearLayout)    inflater.inflate(R.layout.book_grid_view, container, false);
	    context = llLayout.getContext();
	    
	    ButterKnife.inject(faActivity);
	    gridview = (BookScrollerGridView) llLayout.findViewById(R.id.gridview);
		prepareGridView ();
		return llLayout;
	}

	private void fixNumberOfColumns (int method) {
		if (method==1) {
			int orientation = Utils.getOrientation(this);
			int screenSize = Utils.getScreenSize(this);
			boolean tabletFlag = Utils.isTablet(context);
			if(orientation == Configuration.ORIENTATION_LANDSCAPE && tabletFlag)
			{
			    if(screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE)
			    {
			    	gridview.setNumColumns(5);
			    }
			    else
			    {
			    	gridview.setNumColumns(4);
			    }
			}
			else if(orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				gridview.setNumColumns(3);
			}
			else if(orientation == Configuration.ORIENTATION_PORTRAIT && tabletFlag)
			{
			 
			    if(screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE)
			    {
			    	gridview.setNumColumns(4);
			    }
			    else
			    {
			    	gridview.setNumColumns(3);
			    }
			}
		}
		else if (method==2) {
			int screenSize = Utils.getScreenSize(this);
			switch (screenSize) {
			case Configuration.SCREENLAYOUT_SIZE_SMALL:
			     gridview.setNumColumns(3*2);
			break;
			case Configuration.SCREENLAYOUT_SIZE_NORMAL:
			     gridview.setNumColumns(3*2);
			break;
			case Configuration.SCREENLAYOUT_SIZE_LARGE:
			     gridview.setNumColumns(5*2);
			break;
			case Configuration.SCREENLAYOUT_SIZE_XLARGE:
			     gridview.setNumColumns(6*2);
			break;
			}
		}
	}

	private void setClickListener() {
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				BookPreviewItem bookToSend = (BookPreviewItem) gridview.getAdapter().getItem(position);

				//Open an intent when we click a book item.
//				Intent i = new Intent(getActivity(), BookPreviewActivity.class);
//				i.putExtra("BOOK_SENT", bookToSend);
//				startActivity(i);
				
				//Open a dialog view when we click a book item.
				BookPreviewDialog dialog = new BookPreviewDialog(context, bookToSend);
                dialog.setCancelable(true);   
                dialog.show();
			}
		});
	}

	private void prepareGridView() {
		gridview.setImageTextAdapter(new BookBrowserAdapter(context));

//		fixNumberOfColumns(1);

		setClickListener ();
	}

	public static final BookFinderFragment newInstance() {
		 BookFinderFragment f = new BookFinderFragment();
		 return f;
	}
}
