package net.bergice.swiftbooks.utils.render;

import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;

public class MyGLRenderer implements MyGLSurfaceView.Renderer {

	@Override
	public void onSurfaceCreated(GL10 arg0, javax.microedition.khronos.egl.EGLConfig arg1) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}

	@Override
	public void onDrawFrame(GL10 arg0) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void onSurfaceChanged(GL10 arg0, int arg1, int arg2) {
        GLES20.glViewport(0, 0, arg1, arg2);
	}

}